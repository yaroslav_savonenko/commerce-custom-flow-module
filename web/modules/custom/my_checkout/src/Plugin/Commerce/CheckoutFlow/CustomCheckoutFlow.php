<?php

namespace Drupal\my_checkout\Plugin\Commerce\CheckoutFlow;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowWithPanesBase;

/**
 * Custom checkout flow with custom step for invoice details.
 *
 * @CommerceCheckoutFlow(
 *  id = "custom_checkout_flow",
 *  label = @Translation("Custom checkout flow"),
 * )
 */
class CustomCheckoutFlow extends CheckoutFlowWithPanesBase {

  /**
   * {@inheritdoc}
   */
  public function getSteps() {
    return [
      'invoice_details' => [
        'label' => $this->t('Invoice detail'),
        'has_sidebar' => TRUE,
      ],
    ] + parent::getSteps();
  }

}
