<?php

namespace Drupal\my_checkout\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneInterface;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Custom pane for invoice step.
 *
 * @CommerceCheckoutPane(
 *  id = "custom_invoice_step",
 *  label = @Translation("Custom invoice information"),
 *  admin_label = @Translation("Custom invoice information"),
 * )
 */
class CustomInvoiceStep extends CheckoutPaneBase implements CheckoutPaneInterface {

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    $pane_form['description'] = [
      '#markup' => t('Invoice Details'),
    ];
    $pane_form['first_name'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => t('First name'),
    ];
    $pane_form['last_name'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => t('Last name'),
    ];
    $pane_form['invoice_email'] = [
      '#type' => 'email',
      '#required' => TRUE,
      '#title' => t('Invoice email'),
    ];
    $pane_form['invoice_address'] = [
      '#type' => 'textfield',
      '#title' => t('Address'),
    ];
    return $pane_form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitPaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    $values = $form_state->getValue('custom_invoice_step');
    $email = $form_state->getValue(['custom_invoice_step', 'invoice_email']);
    $this->order->setEmail($email);
    $this->order->setData('custom_invoice_vals', $values);
    $this->order->save();
  }

}
