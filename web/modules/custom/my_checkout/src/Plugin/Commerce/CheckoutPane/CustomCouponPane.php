<?php

namespace Drupal\my_checkout\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneInterface;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\commerce_promotion\PromotionOrderProcessor;

/**
 * Custom checkout pane for coupons code with case-sensitivity checking.
 *
 * @CommerceCheckoutPane(
 *  id = "custom_coupon_pane",
 *  label = @Translation("Custom coupon"),
 *  admin_label = @Translation("Custom coupon"),
 * )
 */
class CustomCouponPane extends CheckoutPaneBase implements CheckoutPaneInterface {

  /**
   * The entity type manager interface.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entity_type_manager;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Driver\mysql\Connection
   */
  protected $database;

  /**
   * The PromotionOrderProcessor object.
   *
   * @var \Drupal\commerce_promotion\PromotionOrderProcessor
   */
  protected $promo_order_processor;

  /**
   * CustomCouponPane constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Database\Driver\mysql\Connection $database
   *   The database connection.
   * @param \Drupal\commerce_promotion\PromotionOrderProcessor $promo_order_processor
   *   The promotion order processor service.
   * @param array $configuration
   *   Configuration array implemented from CheckoutPaneBase.
   *   @see \Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase
   * @param $plugin_id
   *   @see \Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase
   * @param $plugin_definition
   *   @see \Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase
   * @param \Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlowInterface $checkout_flow
   *   Checkout flow interface.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, Connection $database, PromotionOrderProcessor $promo_order_processor, array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow) {
    $this->entity_type_manager = $entity_type_manager;
    $this->database = $database;
    $this->promo_order_processor = $promo_order_processor;
    parent::__construct($configuration, $plugin_id, $plugin_definition, $checkout_flow, $entity_type_manager);
  }

  /**
   * Create function.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   DI container.
   * @param array $configuration
   *   Configuration array implemented from CheckoutPaneBase.
   *   @see \Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase
   * @param $plugin_id
   *   @see \Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase
   * @param $plugin_definition
   *   @see \Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase
   * @param \Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlowInterface $checkout_flow
   *   Checkout flow interface.
   *
   * @return CheckoutPaneBase|\Drupal\Core\Plugin\ContainerFactoryPluginInterface|static
   *   Returns container interface.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow = NULL) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('database'),
      $container->get('commerce_promotion.promotion_order_processor'),
      $configuration,
      $plugin_id,
      $plugin_definition,
      $checkout_flow
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    $pane_form['coupon'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Coupon'),
      '#default_value' => '',
      '#required' => FALSE,
    ];
    return $pane_form;
  }

  /**
   * {@inheritdoc}
   */
  public function validatePaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    $values = $form_state->getValue($pane_form['#parents']);
    if (!empty($values['coupon'])) {
      $coupon_code = $values['coupon'];
      $valid = FALSE;
      $query = $this->database->select('commerce_promotion_coupon', 'cpc');
      $query->addField('cpc', 'id');
      $query->condition('cpc.code', $coupon_code, 'LIKE BINARY');
      $coupon_id_from_db = $query->execute()->fetchField();
      if ($coupon_id_from_db) {
        $commerce_coupon_db = $this->entity_type_manager->getStorage('commerce_promotion_coupon')->load($coupon_id_from_db);
        if ($commerce_coupon_db->available($this->order)) {
          $promotion = $commerce_coupon_db->getPromotion();
          $valid = $promotion->applies($this->order) ? TRUE : FALSE;
          if ($valid) {
            foreach ($this->order->coupons->referencedEntities() as $coupon) {
              if ($commerce_coupon_db->id() == $coupon->id()) {
                $form_state->setError($pane_form, $this->t('Coupon already applied to order.'));
              }
            }
          }
        }
        elseif (!$commerce_coupon_db->available($this->order)) {
          $form_state->setError($pane_form, $this->t('Coupon is not active.'));
        }
      }
      else {
        $form_state->setError($pane_form, $this->t('Code is not valid.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitPaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    $values = $form_state->getValue($pane_form['#parents']);
    if (!empty($values['coupon'])) {
      $coupon_code = $values['coupon'];
      $commerce_coupon = $this->entity_type_manager->getStorage('commerce_promotion_coupon')->loadEnabledByCode($coupon_code);
      if ($commerce_coupon) {
        $this->order->coupons[] = $commerce_coupon->id();
        $coupon_order_processor = $this->promo_order_processor;
        $coupon_order_processor->process($this->order);
        $this->order->save();
      }
    }
  }

}
