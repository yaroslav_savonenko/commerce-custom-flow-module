<?php

namespace Drupal\my_checkout\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\state_machine\Event\WorkflowTransitionEvent;

/**
 * Class CustomCheckoutEventSubscriber.
 *
 * @package Drupal\my_checkout\EventSubscriber
 */
class CustomCheckoutEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritDoc}
   */
  public static function getSubscribedEvents() {
    $events = ['commerce_order.contact.pre_transition' => 'toOrderPayment'];
    return $events;
  }

  /**
   * Callback function for commerce_order.contact.pre_transition.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   WorkflowTransitionEvent.
   */
  public function toOrderPayment(WorkflowTransitionEvent $event) {
    $order = $event->getEntity();
    $params = $order->getData('custom_invoice_vals');
    $mail_manager = \Drupal::service('plugin.manager.mail');
    $module = "my_checkout";
    $key = "order_to_payment";
    $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $params['langcode'] = $langcode;
    $to = $params['invoice_email'];
    $mail_manager->mail($module, $key, $langcode, $to, $params, NULL, TRUE);
  }

}
